import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import http from "../utils/http";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    apiToken: null,
    users: []
  },
  mutations: {
    setApiToken(state, apiToken) {
      state.apiToken = apiToken;
    },
    addUser(state, username) {
      state.users.push({ username, status: {} });
    },
    deleteUser(state, username) {
      state.users = state.users.filter(user => user.username !== username);
    },
    setUserStatus(state, { username, status }) {
      const user = state.users.find(user => user.username === username);
      user.status = status;
    }
  },
  actions: {
    setApiToken({ commit }, apiToken) {
      commit("setApiToken", apiToken);
    },
    addUser({ commit, dispatch }, username) {
      commit("addUser", username);
      dispatch("fetchUserStatus", { username });
    },
    deleteUser({ commit }, username) {
      commit("deleteUser", username);
    },
    fetchUserStatus({ commit }, { username }) {
      http.get(`/users/${username}/status`).then(({ data: status }) => {
        commit("setUserStatus", { username, status });
      });
    },
    fetchUsersStatus({ state, dispatch }) {
      state.users.forEach(user => {
        dispatch("fetchUserStatus", user);
      });
    }
  },
  modules: {},
  plugins: [
    createPersistedState({
      key: "gitlab-users-list"
    })
  ]
});
